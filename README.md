:warning: Tous vos travaux en R4.02 sont à enregistrer dans votre dossier : https://gitlab.univ-lille.fr/iut-info-r4.02/2024/groupe-X/nom-prenom/tpX

# Requins et poissons

Mettez-vous en binome pour ce TP en TDD ping pong. Vous enregistrerez votre travail dans le dossier d'un des membres du binome.

Le projet courant est un projet maven, vous pouvez l'importer dans eclipse comme un "Existing Maven project" pour bootstraper votre propre projet ou l'ouvrir directement dans IntellIj.

## L'océan

La simulation de notre océan est basée sur des pas de temps discrets i.e des tours. Elle fonctionne sur une grille rectangulaire. Pour représenter le monde, nous utiliserons un tore, c'est-à-dire que les côtés opposés de la grille sont connectés. Si un individu sort d'un côté du domaine de la simulation, il y rentre immédiatement du côté opposé. Les poissons et les requins se déplacent à chaque tour et interagissent selon l'ensemble des règles suivantes :

* À chaque tour les requins mangent tous les poissons dans les cases adjacentes, s'il n'y a pas de poisson à manger ils bougent, ils ne peuvent pas à la fois manger et bouger
* À chaque tour les poissons bougent de façon aléatoire
* Les poissons et les requins peuvent bouger d'une case dans une seule direction (Nord, Sud, Est, Ouest)
* Les poissons ont un compteur de gestation (5 tours). Si ce compteur arrive à échéance, et si l'une des cases adjacentes est disponible 1 poisson donne naissance à 1 nouveau poisson dans une des cases libres adjacentes, son compteur de gestation est alors réinitialisé.
* Si un poisson n'a pas pu donner naissance à un nouveau poisson par manque de place, au tour suivant il peut encore donner naissance à un bébé.
* Tous les 15 poissons mangés, les requins donnent naissance à un bébé requin.
* Si un requin n'a rien mangé pendant 10 tours, il meurt.
* Il ne peut y avoir qu'un seul individu sur une case
* Si un individu est bloqué il ne se déplace pas

2 représentations possible en mode texte : 
* `-` pour un poisson
* `@` pour un requin

ou en emoji :

* 🐟 : pour un poisson
* 🦈 : pour un requin

## Étape 1

Développer ce simulateur en binôme et en TDD ping pong. 

Le pair-programming ce n'est pas chacun code à tour de rôle mais, on code à 2 et le clavier alterne à tour de rôle.

* *Test* :
    * Lancer le chronomètre
    * Écrire un test
    * Vérifier qu'il est rouge (Tout compile sans erreur mais le test ne passe pas)
    * Commiter avec en message l'intention fonctionnel et d'intention de code
    * push
    * Arrêter le chronomètre
* *Implémentation* :
    * Lancer le chronomètre
    * pull du projet
    * Faire passer le test au vert, et bien évidement, conserver tous les précédents au verts aussi.
    * Commit + push
    * Arrêter le chronomètre
* *Restructuration* :
    * Lancer le chronomètre
    * Restructurer le code
    * Et bien évidement, conserver tous les tests verts
    * Commit + push
    * Arrêter le chronomètre

## Étape 2

Rendre les choses paramétrable.

* Le nombre de tours avant qu'un requin ne soit affamé
* Le nombre de poissons mangé par un requin avant qu'il ne donne naissance
* Le nombre de tours de gestation des poissons
* Les distances de déplacement des poissons et des requins

## Étape 3

* Si un poisson est à moins de X cases d'un requin, il sait qu'il est à porté d'un prédateur, son prochain mouvement sera dans la direction la plus opposée possible. Comme les poissons et les requins ne se déplacent qu'à angle droit, les calcules se feront en distances de Manhattan.
